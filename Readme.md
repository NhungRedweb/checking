Zero Web 
=========

Installation
------------

Run command below to set up environment.

    $ yarn
    or
    $ npm install    

Development
-----------

    $ yarn dev
    or
    $ npm run dev

![YAP](https://media.giphy.com/media/B5a9bkLouElOM/giphy.gif)

Kill Port on Mac/Ubuntu
-----------------------

    sudo lsof -i tcp:8080
    kill ${PID}
