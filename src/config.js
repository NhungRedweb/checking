
module.exports = Object.assign({
  host: process.env.HOST || 'localhost',
  port: process.env.PORT,
  env: process.env.NODE_ENV,
  webpackPort: process.env.WEBPACK_PORT,
  app: {
    title: 'Zero Web',
    description: 'Change the world',
    head: {
      titleTemplate: 'Zero Web',
      meta: [

      ]
    }
  }
});
