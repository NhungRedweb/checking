/** 
 * Note: environment config
 * server: in package.js
 * brower: in webpack cofig file.
*/
let API_URL = 'http://zerozero.tk:4040';

if (process.env.NODE_ENV === 'development') {
  API_URL = 'http://localhost:4040';
}

export default {
  routes: [
    {
      path: '/',
      components: [{
        id: 'components/core/MainHeader',
        props: {}
      }, {
        id: 'components/cards/GridCard',
        props: {
          title: 'Bài Viết Nổi Bật',
          resource_Endpoint: `${API_URL}/api/story`
        }
      }, {
        id: 'components/cards/GridCard',
        props: {
          title: 'UI/UX',
          resource_Endpoint: `${API_URL}/api/story`
        }
      }, {
        id: 'components/cards/GridCard',
        props: {
          title: 'Công Nghệ Web',
          resource_Endpoint: `${API_URL}/api/story`
        }, 
      }, {
        id: 'components/core/Footer',
        props: {}
      }]
    },
    {
      path: '/about',
      components: [{
        id: 'components/core/MainHeader',
        props: {
          
        }
      }]
    },
    {
      path: '/sdasd',
      components: [{
        id: 'components/core/NotFound'
      }]
    },
  ]
}