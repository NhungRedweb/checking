const renderFullPage = (html, preloadedState) => {
  return `
    <!doctype html>
    <html lang="en-us">
      <head>
        <title>Zero Project</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta charSet="utf-8" />
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" crossOrigin="anonymous" />
        <link rel='stylesheet' href='${__DEVELOPMENT__ ? 'http://localhost:3333' : ''}/dist/bundle.css' />
      </head>
      <body>
        <div id="content">${html}</div>
        <script>
          // WARNING: See the following for security issues around embedding JSON in HTML:
          // http://redux.js.org/docs/recipes/ServerRendering.html#security-considerations
          window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState).replace(/</g, '\\u003c')}
        </script>
        <script src='${__DEVELOPMENT__ ? 'http://localhost:3333' : ''}/dist/bundle.js' charSet="UTF-8"></script>
      </body>
    </html>
    `
}

export default renderFullPage;