import express from 'express';
import passport from 'passport';

const router = express.Router();

router.get('/facebook',
  passport.authenticate('facebook', { scope: ['user_friends', 'email', 'public_profile'] }));

router.get('/facebook/return', 
  passport.authenticate('facebook', { failureRedirect: '/' }), 
  (req, res) => {
    res.redirect('/');
});

router.get('/logout', (req, res) => {
  req.session.passport = {};
  res.redirect('/');
});

export default router;