import Express from 'express';
import React from 'react';
import ReactDOM from 'react-dom/server';
import path from 'path';
import http from 'http';
import match from 'react-router/lib/match';
import RouterContext from 'react-router/lib/RouterContext';
import { Provider } from 'react-redux';
import {uniq} from 'lodash';
import axios from 'axios';
import session from 'express-session';
import passport from 'passport';

import './config/passport';
import auth from './middlewares/auth';
import api from './api';
import generateStore from '../app/redux/store';
import config from '../config';
import pageConfig from '../page-configs/page_v1';
import renderFullPage from './helpers/renderFullPage';
import routesApp from '../app/routes';

const app = new Express();
const server = new http.Server(app);

app.use(session({
  name: 'zero.ss.id',
  resave: true,
  secret: 'keyboard cat', 
  cookie: { maxAge: 3600000 * 24 }}
));
app.use(passport.initialize());
app.use(passport.session());

app.use(Express.static(path.join(__dirname, '../../', 'static')));
app.use('/auth', auth);
app.use('/api', api);
/**
 *  Server side rendering
 */
const routes = routesApp();
const store = generateStore({});

app.use('*', (req, res) => {
  match({routes, location: req.originalUrl}, async (error, redirectLocation, renderProps) => {

    if (redirectLocation) {
      res.redirect(redirectLocation.pathname + redirectLocation.search);

    } else if (error) {
      res.status(500);

    } else if (renderProps) {

      store.dispatch({
        type: 'update_login',
        payload: req.session.passport || {}
      })

      const listResource_Endpoint = getListResource_EndPoint(pageConfig, req);

      const listPromise = listResource_Endpoint.map(resource_endpoint => 
        axios.get(resource_endpoint).
          then(response => 
            new Promise((resovle, rej) => resovle({[`${resource_endpoint}`]: response.data})))
      );

      let resourceData;

      try {
        resourceData = await Promise.all(listPromise);
      } catch(error) {
        res.status(500);
      }
      
      store.dispatch({
        type: 'update_resource',
        payload: resourceData[0]
      });

      const components = renderProps.components;

      const component = (
        <Provider store={store}>
          <RouterContext {...renderProps} />
        </Provider>
      );
      
      const html = ReactDOM.renderToString(component);
        
      res.status(200).send(renderFullPage(html, store.getState()));
    }
  });
});

function getListResource_EndPoint(pageConfig, req) {
  let listResource_Endpoint = [];

  for(let i = 0; i < pageConfig.routes.length; i++) {
    if (pageConfig.routes[i].path === req.originalUrl) {
      for (let j = 0; j < pageConfig.routes[i].components.length; j++) {
        if (pageConfig.routes[i].components[j].props.resource_Endpoint) {
          listResource_Endpoint.push(pageConfig.routes[i].components[j].props.resource_Endpoint);
        }
      }
      break;
    }
  }

  return uniq(listResource_Endpoint);
}

function getResourceData() {

}

const { port, app: { title } } = config;

server.listen(port, (errors) => {
  if (errors) {
    console.error(errors);
    return
  }
  console.info('----\n==> ✅  %s is running, talking to API server on %s.', title, port);
});

