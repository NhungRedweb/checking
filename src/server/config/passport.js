import passport from 'passport';
import {Strategy} from 'passport-facebook';
import axios from 'axios';

import { APIUrl } from '../../share/constant';

passport.use(new Strategy({
  clientID: process.env.FACEBOOK_CLIENT_ID,
  clientSecret: process.env.FACEBOOK_CLIENT_SECRET,
  callbackURL: '/auth/facebook/return',
  profileFields: ['id', 'displayName', 'photos', 'email']
}, (accessToken, refreshToken, profile, cb) => {
  const {
    email,
    picture: {
      data: {url}
    }
  } = profile._json;

  axios({
    method: 'post',
    url: `${APIUrl}/api/auth/login/facebook`,
    data: {
      email,
      avatarUrl: url
    }
  }).then(res => cb(null, res.data));
}));

passport.serializeUser((user, cb) => {
  cb(null, user);
});

passport.deserializeUser((user, cb) => {
  cb(null, user);
});