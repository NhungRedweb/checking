import React from 'react';
import {render} from 'react-dom';
import Router from 'react-router/lib/Router';
import browserHistory from 'react-router/lib/browserHistory';
import match from 'react-router/lib/match';
import './styles/index.css';
import routes from './routes';
import { Provider } from 'react-redux';
import generateStore from './redux/store';

// Pick up any initial state sent by the server
const initialState = window.__PRELOADED_STATE__;
delete window.__PRELOADED_STATE__;

const store = generateStore(initialState);

render(
  <Provider store={store}>
    <Router history={browserHistory}>
        { routes() }
    </Router>
  </Provider>,
  document.getElementById('content')
);

if(module.hot)
  module.hot.accept();