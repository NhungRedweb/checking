import { combineReducers } from 'redux';

const mainReducer = combineReducers({
  resourceData,
  loginData
});

export default mainReducer;

function resourceData(state = {}, action) {
  switch (action.type) {
    case 'update_resource':
      return action.payload;
    default: return state;
  }
}

function loginData(state = {}, action) {
  switch (action.type) {
    case 'update_login':
      return action.payload;
    default: return state;
  }
}