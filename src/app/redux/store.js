import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk'

import mainReducer from './reducer';

const generateStore = (initialState) => 
  createStore(mainReducer, initialState, compose(applyMiddleware(thunkMiddleware), window && window.devToolsExtension ? window.devToolsExtension() : f => f));

export default generateStore;