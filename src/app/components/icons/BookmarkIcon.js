import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Bookmark extends Component {
  static propTypes = {
    height: PropTypes.number,
    weight: PropTypes.number
  }

  static defaultProps = {
    height: 24,
    width: 24
  }

  render() {
    const { height, width } = this.props;
    return (
      <svg className="svgIcon-use" width={height} height={width} viewBox="0 0 24 24">
        <path d="M19.385 4h-9.77A2.623 2.623 0 0 0 7 6.615V23.01a1.022 1.022 0 0 0 1.595.847l5.905-4.004 5.905 4.004A1.022 1.022 0 0 0 22 23.011V6.62A2.625 2.625 0 0 0 19.385 4zM21 23l-5.91-3.955-.148-.107a.751.751 0 0 0-.884 0l-.147.107L8 23V6.615C8 5.725 8.725 5 9.615 5h9.77C20.275 5 21 5.725 21 6.615V23z" fillRule="evenodd">
        </path>
      </svg>
    )
  }
}
