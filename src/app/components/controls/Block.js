import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { EditorState, RichUtils } from 'draft-js';
import Option from '../shared/Option';

export default class Block extends Component {
	static propType = {
		editorState: PropTypes.object.isRequired,
		config: PropTypes.object,
		onChange: PropTypes.func.isRequired,        
	}

	_toggleBlockStyle = (style) => {
		const { onChange, editorState } = this.props;
		const newState = RichUtils.toggleBlockType(
			editorState,
			style
		);
		onChange(newState);
	}

	render() {
		const { config } = this.props;
		return (
		<div className='Block'>
			{
			config.options.map((style, index) => {
					const Icon = config[style].icon;
					return (
					<Option key={index}
						onClick={this._toggleBlockStyle}
						value={style}
						>
						<Icon />
					</Option>
					);
			})
			}
		</div>
		);
	}
}


