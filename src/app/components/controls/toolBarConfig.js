import BoldIcon from '../components/controls/icons/BoldIcon';
import ItalicIcon from '../components/controls/icons/ItalicIcon';
import BlockQuoteIcon from '../components/controls/icons/BlockQuoteIcon';
import CodeIcon from '../components/controls/icons/CodeIcon';
import LargeTextIcon from '../components/controls/icons/LargeTextIcon';
import NormalTextIcon from '../components/controls/icons/NormalTextIcon';
import ImageIcon from '../components/controls/icons/ImageIcon';
import VideoIcon from '../components/controls/icons/VideoIcon';
import LinkIcon from '../components/controls/icons/LinkIcon';

export default {
    options: ['Inline', 'Link', 'Block', 'Image', 'Embedded'],
    Inline: {
        options: ['bold', 'italic'],
        bold: { icon: BoldIcon },
        italic: { icon: ItalicIcon }
    },
    Link: {
        options: ['link'],
        link: { icon: LinkIcon }
    },
    Block: {
        options: ['blockquote', 'code-block', 'largeText', 'normalText'],
        blockquote: { icon: BlockQuoteIcon },
        ['code-block']: { icon: CodeIcon },
        largeText: { icon: LargeTextIcon },
        normalText: { icon: NormalTextIcon },
    },
    Image: {
        options: ['image'],
        image: { icon: ImageIcon }
    },
    Embedded: {
        options: ['video'],
        video: { icon: VideoIcon }
    }
}