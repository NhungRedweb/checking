import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { EditorState, RichUtils } from 'draft-js';
import Option from '../shared/Option';

export default class Inline extends Component {
    static propType = {
        editorState: PropTypes.object.isRequired,
        config: PropTypes.object,
        onChange: PropTypes.func.isRequired,        
    }
    
    _toggleInlineStyle = (style) => {
        const { onChange, editorState } = this.props;
        const newStyle = style.toUpperCase()
        const newState = RichUtils.toggleInlineStyle(editorState, newStyle);
        onChange(newState);
    }

    render() {
        const { config } = this.props;
        return (
            <div className='Inline'>
                {
                    config.options.map((style, index) => {
                        const Icon = config[style].icon;
                        return (
                            <Option key={index}
                                onClick={this._toggleInlineStyle}
                                value={style}
                                >
                                <Icon />
                            </Option>
                        );
                    })
                }
            </div>
        );
    }
}
