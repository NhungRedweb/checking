import Block from './Block';
import Inline from './Inline';
import Embedded from './Embedded';
import Link from './Link';
import Image from './Image';

export default {
    Block,
    Inline,
    Embedded,
    Link,
    Image
};