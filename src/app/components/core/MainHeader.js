import React, { Component } from 'react';
import PropsType from 'prop-types';
import { connect } from 'react-redux';
import Link from 'react-router/lib/Link';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

import LoginModal from './LoginModal';

class MainHeader extends Component {
  static propsType = {
    loginData: PropsType.object
  }

  state = {
    showModal: false,
  }

  closeLoginModal = () => {
    this.setState({ showModal: false });
  }

  showLoginModal = () => {
    this.setState({ showModal: true });
  }

  render() {
    const { 
      loginData: {
        user: userData
      } 
    } = this.props;
    
    const { showModal } = this.state;

    return (
      <div className='core-mh'>
        <div className='container '>
          <div className='core-mh-metabar'>
            <Link to='/' className='metabar-logo'>
              Zero
            </Link>
            <div className='metabar-member'>
              <span ><svg width="25" height="25" viewBox="0 0 25 25"><path d="M20.067 18.933l-4.157-4.157a6 6 0 1 0-.884.884l4.157 4.157a.624.624 0 1 0 .884-.884zM6.5 11c0-2.62 2.13-4.75 4.75-4.75S16 8.38 16 11s-2.13 4.75-4.75 4.75S6.5 13.62 6.5 11z"></path></svg></span>
              {!userData ? <a className='pointer' onClick={this.showLoginModal}>Đăng Nhập</a> : 
                <UserPopup
                  userData={userData}
                />
              }
            </div>
          </div>
          <nav className='core-mh-nav'>
            <ul>
              <li><a>Trang Chủ</a></li>
              <li><a>Cuộc sống</a></li>
              <li><a>Trải nghiệm</a></li>
              <li><a>Du lịch</a></li>
              <li><a>Công nghệ</a></li>
              <li><a>Sáng tạo</a></li>
              <li><a>Xem tất cả</a></li>
            </ul>
          </nav>
          <div className='core-mh-content'>
            <h1>Đọc và lưu lại những bài viết một cách dễ dàng</h1>
            <p>Khám phá những bài viết mới, những tác giả đầy tâm huyết và chia sẻ những trải nghiệm với mọi người vì Zero được thiết kế dành cho bạn.</p>
            <button>Bắt Đầu</button>
          </div>
        </div>
        <LoginModal
          show={showModal}
          onHide={this.closeLoginModal}
        />
      </div>
    )
  }
}

class UserPopup extends Component {
  static propsType = {
    userData: PropsType.object
  }

  state = {
    show: false
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside)
  }

  handleClickOutside = (event) => {
    if(this.popup && !this.popup.contains(event.target)) {
      this.setState({ show: false });
    }
  }

  onToggle = () => {
    this.setState({ show: !this.state.show })
  }

  render() {
    const {
      avatarurl
    } = this.props.userData;

    const style = {
      visibility: this.state.show ? 'visible' : 'hidden'
    }

    return (
      <div className='core-mh-avartar' onClick={this.onToggle} ref={(node) => this.popup = node}>
        <image src={avatarurl}/>
        <div className='core-mh-userpopup' style={style}>
          <ul>
            <li><a href='/'>Profile</a></li>
            <li><a href='/'>Cài Đặt</a></li>
            <li><a href='/auth/logout'>Đăng Xuất</a></li>
          </ul>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    loginData: state.loginData
  } 
}

export default connect(mapStateToProps)(MainHeader);
