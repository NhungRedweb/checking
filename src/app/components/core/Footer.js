import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Footer extends Component {
  
  render() {
    return (
      <div className='core-ft container'>
        <a>Tìm hiểu thêm</a>
        <a>Trợ giúp</a>
        <a>Điều khoản</a>
        <a>Liên hệ</a>
      </div>
    )
  }
}

export default Footer;