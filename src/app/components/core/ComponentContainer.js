import React, { Component } from 'react';
import PropsType from 'prop-types';
import {connect} from 'react-redux';

import componentLoader from '../../utils/componentLoader';

class ComponentContainer extends Component {
  static propsType = {
    id: PropsType.string.isRequired,
    componentInstanceId: PropsType.string.isRequired,
    componentProps: PropsType.object,
    resourceData: PropsType.any
  }

  render() {
    const {
      componentInstanceId,
      componentProps,
      id,
      resourceData,
      componentProps: {
        resource_Endpoint
      } = {}
    } = this.props;

    const ComponentInstance = componentLoader(componentInstanceId);
    let data;
    if (resource_Endpoint) {
      data = resourceData[resource_Endpoint];
    }

    return (
      <div className='ComponentContainer'>
        <ComponentInstance {...componentProps} id={id} resourceData={data}/>
      </div>
    );
  }
}

export default connect(mapStateToProps)(ComponentContainer);

function mapStateToProps(state) {
  return {
    resourceData: state.resourceData
  }
}
