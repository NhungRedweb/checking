import React, { Component } from 'react';
import PropsType from 'prop-types';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

import { CloseIcon, FacebookIcon, GoogleIcon } from '../icons';

class LoginModal extends Component {
  static propsType = {
    show: PropsType.bool,
    onHide: PropsType.func
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside)
  }

  handleClickOutside = (event) => {
    if(this.body && !this.body.contains(event.target)) {
      this.props.onHide();
    }
  }

	render() {
    if (!this.props.show) {
      return null;
    }

		return (
      <div className='core-lm-main'>
        <div className='core-lm-fade' />
        <div className='core-lm-dialog'>
          <ReactCSSTransitionGroup
            transitionName="example"
            transitionAppear={true}
            transitionAppearTimeout={1000}
            transitionLeaveTimeout={500}
            transitionEnterTimeout={500}
          >
            <div className='core-lm-body' key='dsa' ref={(node) => this.body = node}>
              <button className='button-close' onClick={() => this.props.onHide()}>
                <CloseIcon />
              </button>
              <section className='core-lm-part core-lm-part--left'>
                <Slogan />
              </section>
              <div className='core-lm-divider hide-md'></div>
              <section className='core-lm-part core-lm-part--right'>
                <LoginForm />
              </section>
            </div>
          </ReactCSSTransitionGroup> 
        </div>
      </div>
		)
	}
}

const Slogan = (props) => (
  <div className='core-lm-slogan'>
    <h1>Project Zero</h1>
    <p>Viết về những kinh nghiệm quý giá mà bạn may mắn trải nghiệm được </p>
  </div>
)

class LoginForm extends Component {

  render() {
    return (
      <div className='core-lm-form'>
        <h2>Nhập địa chỉ email để đăng nhập hoặc đăng ký mới</h2>

        <div className='core-lm-form--btns'>
          <SocialLoginButton 
            icon={<FacebookIcon />}
            title='Đăng nhập với Facebook'
            className='facebook'
            href='/auth/facebook'
          />
          <SocialLoginButton 
            icon={<GoogleIcon />}
            title='Đăng nhập với Google'
            className='google'
          />
        </div>
      </div>
    )
  }
}

const SocialLoginButton = (props) => (
  <a className={`social-btn ${props.className}`} href={props.href}>
    <span className='social-btn-icon'>{props.icon}</span>
    <span className='social-btn-title'>{props.title}</span>
  </a>
)

export default LoginModal;