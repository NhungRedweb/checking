import React, {Component} from 'react';
import PropTypes from 'prop-types';

export default class Option extends Component {
    static propType = {
        onClick: PropTypes.func.isRequired,
        value: PropTypes.string.isRequired
    }

    _onClick = () => {
        const { onClick, value } = this.props;
        onClick(value);
    }

    render() {
        const { children } = this.props;
        return (
            <button className='Option-Button'
                onClick={this._onClick}>
                <span>
                {children}
                </span>
            </button>
        );
    }
}