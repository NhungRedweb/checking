import React, { Component } from 'react';
import PropTypes from 'prop-types';

class CategoryTitle extends Component {
  static propTypes = {
    title: PropTypes.string
  }
  
  render() {
    return (
      <div className='GridCard-title'>
        <div className='GridCard-title--text'>
          <h1>{this.props.title}</h1>
        </div>
        <div className='GridCard-title--viewAll'>
          <a href='#'>Xem Tất Cả</a>
        </div>
      </div>
    )
  }
}

export default CategoryTitle;