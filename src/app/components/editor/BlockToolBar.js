import PropTypes from 'prop-types';
import React, { Component } from 'react';

import StyleButton from './StyleButton';
import BlockquoteIcon from './icons/BlockquoteIcon';
import LargeTextIcon from './icons/LargeTextIcon';
import NormalTextIcon from './icons/NormalTextIcon';

export default class BlockToolbar extends Component {
   render() {
      return (
         <div className="BlockToolbar">
            {
            BLOCK_STYLES.map((inlineItem, index) => (
                <StyleButton 
                    key={index}
                    style={inlineItem.style}
                    icon={inlineItem.icon}
                    description={inlineItem.description}
                />
            ))
            }
         </div>
      );
   }
};

const BLOCK_STYLES = [
    {
        style: 'BOLD',
        description: 'Bold',
        icon: (<BlockquoteIcon />),
    },
    {
        style: 'ITALIC',
        description: 'Italic',
        icon: (<LargeTextIcon />),
    },
    {
        style: 'ITALIC',
        description: 'Italic',
        icon: (<NormalTextIcon />),
    }
];
  