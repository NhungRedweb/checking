import React, {Component} from 'react';

export default class VideoIcon extends Component {
    render() {
        return ( 
            <svg className="svgIcon-use" width="21" height="21" viewBox="0 0 21 21"><path d="M18.8 11.536L9.23 5.204C8.662 4.78 8 5.237 8 5.944v13.16c0 .708.662 1.165 1.23.74l9.57-6.33c.514-.394.606-1.516 0-1.978zm-.993 1.45l-8.294 5.267c-.297.213-.513.098-.513-.264V7.05c0-.36.218-.477.513-.264l8.294 5.267c.257.21.257.736 0 .933z" fillRule="evenodd"></path></svg>            
        );
    }
}