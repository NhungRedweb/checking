import React, {Component} from 'react';

export default class CodeIcon extends Component {
    render() {
        return ( 
            <svg className="svgIcon-use" width="21" height="21" viewBox="0 0 21 21"><g fillRule="evenodd"><path d="M9.826 7.698l-4.828 4.828 4.828 4.828.652-.7-4.08-4.128L10.53 8.4"></path><path d="M14.514 8.4l4.177 4.126-4.17 4.127.7.7 4.83-4.827-4.83-4.828"></path></g></svg>        
        );
    }
}
