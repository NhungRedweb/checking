import PropTypes from 'prop-types';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import InlineToolbar from './InlineToolbar';
import BlockToolBar from './BlockToolBar';
import HyperLink from './HyperLink';
import { getSelection, getSelectionRect } from '../utils/editorUtil';

export default class Toolbar extends Component {
    static propTypes = {
        editorState: PropTypes.object,
        editorNode: PropTypes.object
    };

    componentDidUpdate() {
        const { editorState, editorNode } = this.props;
        const nativeSelection = getSelection(window);
        if (editorState.getSelection().isCollapsed() || !nativeSelection.rangeCount) {
            return;
        }

        const selectionBoundary = getSelectionRect(nativeSelection);
        const toolbarNode = ReactDOM.findDOMNode(this);
        const toolbarBoundary = toolbarNode.getBoundingClientRect();

        const parent = ReactDOM.findDOMNode(editorNode);
        const parentBoundary = parent.getBoundingClientRect();

        /*
        * Main logic for setting the toolbar position.
        */
        toolbarNode.style.top = `${(selectionBoundary.top - parentBoundary.top - toolbarBoundary.height - 8)}px`;
        
        toolbarNode.style.width = `${toolbarBoundary.width}px`;
        
        const widthDiff = selectionBoundary.width - toolbarBoundary.width;

        if (widthDiff >= 0) {
            toolbarNode.style.left = `${widthDiff / 2}px`;
        } else {
            const left = (selectionBoundary.left - parentBoundary.left);
            toolbarNode.style.left = `${left + (widthDiff / 2)}px`;
        }
    }

    render() {
        const {editorState} = this.props;
        const isOpen = !editorState.getSelection().isCollapsed();
        return (
            <div className={`Toolbar ${(isOpen ? 'Toolbar-open' : '')}`}>
                <InlineToolbar />
                <BlockToolBar />
                <HyperLink />
            </div>
        );
    }
} 