import React, { Component } from 'react';
import _ from 'lodash';
import { 
   Editor,
   EditorState,
   getDefaultKeyBinding, 
   KeyBindingUtil, 
   RichUtils, 
   SelectionState, 
   Modifier 
} from 'draft-js';

import {
   getCurrentBlock,
   addNewBlockAt,
   resetBlockWithType
} from '../utils/editorUtil';

import {
   Block,
   Entity as E,
   HANDLED,
   NOT_HANDLED,
   KEY_COMMANDS 
} from '../utils/contants';

import Toolbar from './Toolbar';
import SideToolBar from './SideToolBar';

const { hasCommandModifier } = KeyBindingUtil;

export default class ZeroEditor extends Component {

   state = {
      editorState : EditorState.createEmpty()
   }

   _onChange = (editorState) => {
      this.setState({editorState});
   } 

   handleKeyCommand = (command) => {
      return 'not-handled';
   }

   _handleReturn = (e) => {
      const { editorState } = this.state;

      const currentBlock = getCurrentBlock(editorState);
      const blockType = currentBlock.getType();
      const currentBlocklenght = currentBlock.getLength();
      if (currentBlocklenght === 0) {
         switch (blockType) {
         case Block.UL:
         case Block.OL:
         case Block.BLOCKQUOTE:
         case Block.BLOCKQUOTE_CAPTION:
         case Block.CAPTION:
         case Block.TODO:
         case Block.H2:
         case Block.H3:
         case Block.H1:
            this._onChange(resetBlockWithType(editorState, Block.UNSTYLED));
            return HANDLED;
         default:
            return NOT_HANDLED;
         }
      }
   }

   render() {
      const { editorState } = this.state;
      const controlProps = {
         editorState,
         onChange: this._onChange
      };
      const placeholder = '   Tell your story ...'
      return (
         <div className="NewStory">
         <div className="ZeroEditor">
            <Editor 
               ref={(node) => this.editorNode = node }
               editorState={editorState} 
               onChange={this._onChange} 
               placeholder={placeholder}
               blockStyleFn={myBlockStyleFn}
               handleKeyCommand={this.handleKeyCommand}
               handleReturn={this._handleReturn}
            />
            <SideToolBar 
              editorState={editorState}
              editorNode={this.editorNode}
            />
            <Toolbar 
              editorState={editorState}
              editorNode={this.editorNode}
            />
         </div>
         </div>
      );
   }
}

const myBlockStyleFn = (contentBlock) => {
   const type = contentBlock.getType();
   if (type === 'blockquote') {
      return 'BlockquoteCustom';
   } else if (type === 'code-block') {
      // return 'CodeBlockCustom';
   }
}
