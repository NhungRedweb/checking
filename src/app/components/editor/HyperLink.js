import PropTypes from 'prop-types';
import React, { Component } from 'react';

import LinkIcon from './icons/LinkIcon';

export default class HyperLink extends Component {
   render() {
      return (
         <div className="HyperLink">
             <span>
                 <LinkIcon />
             </span>
         </div>
      );
   }
};