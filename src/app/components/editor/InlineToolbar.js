import PropTypes from 'prop-types';
import React, { Component } from 'react';

import StyleButton from './StyleButton';
import BoldIcon from './icons/BoldIcon';
import ItalicIcon from './icons/ItalicIcon';

export default class InlineToolbar extends Component {
   render() {
      return (
         <div className="InlineToolbar">
            {
            INLINE_STYLES.map((inlineItem, index) => (
                <StyleButton 
                    key={index}
                    style={inlineItem.style}
                    icon={inlineItem.icon}
                    description={inlineItem.description}
                />
            ))
            }
         </div>
      );
   }
};

const INLINE_STYLES = [
    {
        style: 'BOLD',
        description: 'Bold',
        icon: (<BoldIcon />),
    },
    {
        style: 'ITALIC',
        description: 'Italic',
        icon: (<ItalicIcon />),
    }
];
  