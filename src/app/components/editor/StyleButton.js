import PropTypes from 'prop-types';
import React, { Component } from 'react';
import BoldIcon from './icons/BoldIcon';

export default class StyleButton extends Component {
	static propTypes = {
		style: PropTypes.string.isRequired,
		icon: PropTypes.object.isRequired,
		description: PropTypes.string
	}

	render() {
		const { icon } = this.props;
		return (
			<span className="StyleButton">
				{icon}
			</span>
		);
	}
}