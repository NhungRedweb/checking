import React, { Component } from 'react';
import PropsType from 'prop-types';
import Link from 'react-router/lib/Link';

import StarIcon from '../icons/StarIcon';
import BookmarkIcon from '../icons/BookmarkIcon';

class Card extends Component {
  static propsType = {
    headline: PropsType.string,
    description: PropsType.string,
    storyimage: PropsType.object,
    author: PropsType.object
  }

  render() {
    const {
      headline,
      description,
      storyimage: {
        filename,
        caption
      },
      author: {
        avatar,
        name,
        description: authorDescription
      }
    } =  this.props;

    const style = {
      backgroundImage: `url(${filename})`
    }

    return (
      <div className='gutter col-sm-4'>
        <div className='Card'>
          <div className='Card-thumbnail' style={style}>
            <div className='Card-author'>
              <img className='Card-author--avatar' src={avatar}/>
              <div className='Card-author--content'>
                <h4>{name}</h4>
                <p>{authorDescription}</p>
              </div>
            </div>
          </div>
          <div className='Card-content'>
            <div>
              <Link to='/about' className='story-link'>
                <h1 className='Card-content--headline'>{headline}</h1>
              </Link>
              <p className='Card-content--description'>{description}</p>
            </div>
            <Interaction />
          </div>   
        </div>
      </div>
    );
  }
}

class Interaction extends Component {
  render() {
    return (
      <div className='Interaction'>
        <div className='Interaction-left'>
          <span className='Interaction-left--item'>
            <StarIcon/>
            <a> 11</a>
          </span>
          <span className='Interaction-left--item'>
            <StarIcon />
            <a> 11</a>
          </span>
        </div>
        <span className='Interaction-bookmark'>
          <BookmarkIcon />
        </span>
      </div> 
    )
  }
}

export default Card;