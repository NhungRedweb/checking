import React, { Component } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import { connect } from 'react-redux';

import Card from './Card';
import CategoryTitle from '../shared/CategoryTitle';

class GridCard extends Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
    title: PropTypes.string,
    resourceData: PropTypes.array
  }

  componentWillMount() {
    console.log(this.props.id)
  }
  
  render() {
    const {
      title,
      resourceData = []
    } = this.props;
    
    return (
      <div className='container'>
        <div className='row'>
          <div className='col-sm-12 GridCard'>
            <CategoryTitle title={title}/>
            { resourceData.map((item, index) => <Card {...item} key={index}/>)}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({ data: state.storyReducer})

export default connect(mapStateToProps)(GridCard);