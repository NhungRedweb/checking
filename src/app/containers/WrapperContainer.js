import React, { Component } from 'react';
import PropsType from 'prop-types';

import ComponentContainer from '../components/core/ComponentContainer';

class WrapperContainer extends Component {
  static propsType = {
    components: PropsType.array.isRequired,
    id: PropsType.string.isRequired
  }

  _renderComponent = () => {
    const { id, components } = this.props;
    return components.map((Comp, index) => 
      <ComponentContainer key={index} componentInstanceId={Comp.id} componentProps={Comp.props} id={`${id}.components[${index}]`}/>); 
  }

  render() {
    return (
      <div className='wrapppppp'>
        {this._renderComponent()}
      </div>
    )
  }
}

export default WrapperContainer;