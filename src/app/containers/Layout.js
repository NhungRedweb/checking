import React, { Component } from 'react';

export class Layout extends Component {
  render() {
    return (
      <div>
        <div className=''>
        {this.props.children}
        </div>
      </div>
    );
  }
}