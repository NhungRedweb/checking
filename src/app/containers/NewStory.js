import React, { Component } from 'react';
import ZeroEditor from '../components/editor/Editor';


export class NewStory extends Component {
  render() {
    return (
      <div className="NewStory">
        <div className="UserInfor">
          <div className="avatar">
            <img src='https://cdn-images-1.medium.com/fit/c/32/32/0*HvcNj_ZRHLX8TcGn.'/>
          </div>
          <div className="infor">
            <div>Hoang Tran</div>
            <div>Draft</div>
          </div>
        </div>
        <ZeroEditor />
      </div>
    );
  }
}