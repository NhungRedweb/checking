import React, { Component } from 'react';
import Route from 'react-router/lib/Route';
import IndexRoute from 'react-router/lib/IndexRoute';

import PageConfig from '../page-configs/page_v1';
import { Layout } from './containers';
import WrapperContainer from './containers/WrapperContainer';

const _wrapeeComponent = (components, id) => {
  class Wrapper extends Component {

    render() {
      return <WrapperContainer components={components} id={id}/>
    }
  }

  return Wrapper;
}

const _generatRoutes = (routes) => {
  return routes.map((route, index) => {
    const Component = _wrapeeComponent(route.components, `routes[${index}]`);

    return (
      <Route
        key={index} 
        path={route.path} 
        component={Component}
      />
    ); 
  })

}

export default () => (
  <Route component={Layout}>
  {
    _generatRoutes(PageConfig.routes)
  }
    {/* <IndexRoute component={LandingPage}/>
    <Route path="new-story" component={NewStory}/> */}
  </Route>
);
