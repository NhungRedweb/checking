const components = {
  'components/core/MainHeader': require('../components/core/MainHeader').default,
  'components/core/Footer': require('../components/core/Footer').default,
  'components/core/NotFound': require('../components/core/NotFound').default,

  'components/cards/GridCard': require('../components/cards/GridCard').default
}

export default (id) => {
  return components[id]
}