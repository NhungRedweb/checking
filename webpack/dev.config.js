import webpack from 'webpack';
import path from 'path';
import config from '../src/config';
import ExtractTextPlugin from 'extract-text-webpack-plugin';

const assetsPath = path.resolve(__dirname, '../static/dist');
const {
  host,
  webpackPort
} = config;

const webpackConfig = {
  entry: {
    main: [
      'webpack-hot-middleware/client?path=http://' + host + ':' + webpackPort + '/__webpack_hmr',
      'webpack/hot/dev-server',
      'babel-polyfill',
      './src/app/client.js'
    ]
  },
  devtool: 'eval',
  output: {
    path: assetsPath,
    filename: 'bundle.js',
    publicPath: 'http://' + host + ':' + webpackPort + '/dist/'
  },
  module: {
    rules: [
      {
        test: /\.js$|\.jsx$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: {
          cacheDirectory: true,
          presets: [ 'react-hmre' ]
        }
      },
      {test: /\.css$/, loader: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: [{
          loader: 'css-loader',
          options: {importLoaders: 1}
        },
        {
          loader: 'postcss-loader',
          options: {
            config: {
              path: 'webpack/postcss.config.js'
            }
          }
        }
      ]
      })},
    ],
  },
  resolve: {
    extensions: [ '.js', '.json', '.jsx', '.css'],
    modules: [
      'src',
      'node_modules'
    ]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new ExtractTextPlugin('bundle.css'),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.EnvironmentPlugin({
      NODE_ENV: "development",
    })
  ],
};

export default webpackConfig;
