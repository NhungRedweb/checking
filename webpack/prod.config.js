import webpack from 'webpack';
import path from 'path';
import config from '../src/config';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import PurifyCSSPlugin from 'purifycss-webpack-plugin';

const assetsPath = path.resolve(__dirname, '../static/dist');
const {
  host,
  webpackPort
} = config;

const webpackConfig = {
  entry: {
    main: [
      './src/app/client.js'
    ]
  },
  output: {
    path: assetsPath,
    filename: 'bundle.js',
    publicPath: 'http://' + host + ':' + webpackPort + '/dist/'
  },
  module: {
    rules: [
      {
        test: /\.js$|\.jsx$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {test: /\.css$/, loader: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: [{
          loader: 'css-loader'
        },
        {
          loader: 'postcss-loader',
          options: {
            config: {
              path: 'webpack/postcss.config.js'
            }
          }
        }
      ]
      })},
    ],
  },
  resolve: {
    extensions: [ '.js', '.json', '.jsx', '.css'],
    modules: [
      'src',
      'node_modules'
    ]
  },
  plugins: [
    new ExtractTextPlugin('bundle.css'),
    new PurifyCSSPlugin({
      purifyOptions: { info: true, minify: true },
      paths: [
        'src/app/client.js'
      ]
    }),
    // optimizations
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
        screw_ie8: true,
        sequences: true,
        dead_code: true,
        drop_debugger: true,
        comparisons: true,
        conditionals: true,
        evaluate: true,
        booleans: true,
        loops: true,
        unused: true,
        hoist_funs: true,
        if_return: true,
        join_vars: true,
        cascade: true,
        drop_console: true
      },
      output: {
        comments: false
      }
    })
  ],
};

export default webpackConfig;
