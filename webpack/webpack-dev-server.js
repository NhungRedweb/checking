import express from 'express';
import webpack from 'webpack';

import config from '../src/config';
import webpackConfig from './dev.config';

const {
  host,
  webpackPort
} = config;

const compiler = webpack(webpackConfig);
const serverOptions = {
  contentBase: 'http://' + host + ':' + webpackPort,
  // quiet: true,
  noInfo: false,
  hot: true,
  inline: true,
  lazy: false,
  publicPath: webpackConfig.output.publicPath,
  headers: {'Access-Control-Allow-Origin': '*'},
  stats: {
    assets: true,
    colors: true,
    version: false,
    hash: false,
    timings: true,
    chunks: false,
    chunkModules: false
  }
};

const app = new express();

// https://github.com/webpack/loader-utils/issues/56
process.noDeprecation = true

app.use(require('webpack-dev-middleware')(compiler, serverOptions));
app.use(require('webpack-hot-middleware')(compiler));

app.listen(webpackPort, (errors) => {
  if (errors) {
    console.error(errors);
    return;
  }
  console.info('==> 🚧  Webpack development server listening on port %s', webpackPort);
});
