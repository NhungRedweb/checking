#!user/bin/env node
require('../server.babel');
global.__DEVELOPMENT__ = process.env.NODE_ENV !== 'production';
global.window = undefined;

if (__DEVELOPMENT__) {
  if (!require('piping')({
      hook: true,
      ignore: /(\/\.|~$.json|\.scss$)/i
    })) {
    return;
  }
}
require('../src/server');
